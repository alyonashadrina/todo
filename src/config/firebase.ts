import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyBCQeW0tAwciJC1ScuzyuJxQdbPGPvGcrU",
    authDomain: "todo-c5e20.firebaseapp.com",
    databaseURL: "https://todo-c5e20.firebaseio.com",
    projectId: "todo-c5e20",
    storageBucket: "todo-c5e20.appspot.com",
    messagingSenderId: "192876669049",
    appId: "1:192876669049:web:9f141771750147d9"
};

firebase.initializeApp(firebaseConfig);

export default firebase;