import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./app/store";

import Layout from "./app/layout/Layout";


const App = () => {
  return (
      <Provider store={store}>
          <BrowserRouter>
            <Layout />
          </BrowserRouter>
      </Provider>
  );
};


ReactDOM.render(<App />, document.getElementById('root'));
