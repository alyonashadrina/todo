import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';

import board from './board/state/reducer';
import boardList from './boardList/state/reducer';
import auth from './auth/state/reducer';

const rootReducer = combineReducers({
    board,
    boardList,
    firebase: firebaseReducer,
    auth: auth
});

export default rootReducer;