import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_REQUEST, SIGNOUT_REQUEST } from "./state/types";

const Auth = () => {
    const dispatch = useDispatch();

    const handleSignIn = () => {
        dispatch({ type: LOGIN_REQUEST, payload: { email: 'test@test.com', password: '123456' } });
    };

    const handleSignOut = () => {
        dispatch({ type: SIGNOUT_REQUEST });
    };

    const loginError = useSelector((state: {auth: { error: string }}) => state.auth.error);

    return (
        <div>
            Auth
            <button onClick={handleSignIn}>Sign In</button>
            <button onClick={handleSignOut}>Sign Out</button>
            { loginError }
        </div>
    )
};

export default Auth;