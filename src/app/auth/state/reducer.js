import * as types from './types';


const initialState = {
    error: null
};

export default function authReducer(state = initialState, { type, payload }) {
    switch (type) {
    case types.LOGIN_SUCCESS:
        return {
            ...state,
            error: null,
        };
    case types.LOGIN_ERROR:
        console.log('payload', payload);
        return {
            ...state,
            error: payload,
        };
    default:
        return state;
    }
}
