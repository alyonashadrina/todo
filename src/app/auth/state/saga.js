import { call, put, takeLatest } from 'redux-saga/effects';

import { signIn, signOut } from './actions';
import { LOGIN_REQUEST, SIGNOUT_REQUEST } from './types';


function* signInRequest({ payload }) {
    try {
        const { data } = yield call(signIn, payload);
        // yield put(makeAddBoardSuccess(data.name));
    } catch (e) {
        console.warn('signInRequest error: ', e);
    }
}

function* signOutRequest({ payload }) {
    try {
        const { data } = yield call(signOut, payload);
        // yield put(makeAddBoardSuccess(data.name));
    } catch (e) {
        console.warn('signOutRequest error: ', e);
    }
}

export default function* watchBoardSaga() {
    yield takeLatest(LOGIN_REQUEST, signInRequest);
    yield takeLatest(SIGNOUT_REQUEST, signOutRequest);
};
