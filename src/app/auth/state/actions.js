import { getFirebase } from 'react-redux-firebase';
import {
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    SIGNOUT_SUCCESS,
} from './types';

export const signIn = ( payload ) => {
    const firebase = getFirebase();

    return firebase.auth().signInWithEmailAndPassword(
        payload.email,
        payload.password,
    ).then(() => {
        return { type: LOGIN_SUCCESS };
    }).catch((e) => {
        console.warn('signIn errorewrer:', e.message);
        return { type: LOGIN_ERROR, payload: e.message };
    })
};

export const signOut = ( ) => {
    const firebase = getFirebase();

    return firebase.auth().signOut().then(() => {
        return { type: SIGNOUT_SUCCESS };
    }).catch((e) => {
        console.warn('signOut error:', e);
        return { type: LOGIN_ERROR };
    })
};