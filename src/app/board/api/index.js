import axios from 'axios';
import { BASE_API_URI, API_URIS } from '../../../config/index';

export const getBoard = (id) => axios(BASE_API_URI + API_URIS.board + '/' + id + '.json');

export const putBoard = ({ id, data }) => axios.put(BASE_API_URI + API_URIS.board + '/' + id + '/boardColumns.json', data);

export const putSettings = ({ id, data }) => axios.put(BASE_API_URI + API_URIS.board + '/' + id + '/settings.json', data);

