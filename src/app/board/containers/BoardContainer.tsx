import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RouteComponentProps, withRouter } from "react-router";

import { getBoardColumns, isLoading } from '../state/selectors';
import Board from "../components/Board";
import { GET_BOARD_REQUEST, POST_BOARD } from '../state/types';
import { taskType } from "../types";


const BoardContainer = ({ match }: RouteComponentProps<{ id: string }>) => {

    const board = useSelector(state => getBoardColumns(state))
    const loading = useSelector(state => isLoading(state))

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch({ type: GET_BOARD_REQUEST, payload: match.params.id })
    }, []);

    const updateBoard = (tasks: Array<taskType>) => {
        dispatch({ type: POST_BOARD, payload: { data: tasks, id: match.params.id} })
    };

    if (loading) {
        return (
            <div>loading</div>
        )
    }

    return (
        <Board data={board} updateBoard={updateBoard}/>
    )
};

export default withRouter(BoardContainer);
