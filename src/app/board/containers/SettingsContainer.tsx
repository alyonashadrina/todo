import React, {useState, useEffect, ChangeEvent} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { withRouter, RouteComponentProps } from "react-router";

import { getBoardSettings, isLoading } from '../state/selectors';
import {
    POST_SETTINGS,
} from '../state/types';


const SettingsContainer = ({ match }: RouteComponentProps<{ id: string }>) => {

    const settings = useSelector(state => getBoardSettings(state));
    const loading = useSelector(state => isLoading(state));

    const [name, setName] = useState(settings ? settings.name : '');

    const dispatch = useDispatch();

    useEffect(() => {
        setName(settings ? settings.name : '')
    }, [settings]);

    const updateSettings = () => {
        dispatch({
            type: POST_SETTINGS,
            payload: {
                data: { name: name },
                id: match.params.id,
            }
        })

    };

    const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };

    if (loading) {
        return (
            <div>loading</div>
        )
    }

    return (
        <div>
            <input defaultValue={name} onChange={handleNameChange}/>
            <button onClick={updateSettings}>Update settings</button>
        </div>
    )
};

export default withRouter(SettingsContainer);
