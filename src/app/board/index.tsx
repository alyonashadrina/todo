import React, { ChangeEvent } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { withRouter, Link, RouteComponentProps } from "react-router-dom";

import Board from "./containers/BoardContainer";
import Settings from "./containers/SettingsContainer";


type routeProps = {
    path: string;
    component: JSX.Element;
    label: string;
};

const routes = [
    {
        path: "/",
        component: <Board />,
        label: 'Board',
    },
    {
        path: "/settings",
        component: <Settings />,
        label: 'Settings',
    },
];

const TabNav = ({ match }: RouteComponentProps) => {

    const [value, setValue] = React.useState(0);

    const handleChange = (event: ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    return (
        <>
            <Tabs
                variant="fullWidth"
                value={value}
                onChange={handleChange}
            >
                {
                    routes.map((item: routeProps, i: number) => (
                            <Tab
                                label={
                                    <Link to={match.url + item.path}>
                                        {item.label}
                                    </Link>
                                }
                                key={i}
                            />
                    ))
                }
            </Tabs>
            {
                routes.map((item: routeProps, i: number) => (
                    value === i && item.component
                ))
            }
        </>
    )
};

export default withRouter(TabNav);