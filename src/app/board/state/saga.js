import { call, put, takeLatest } from 'redux-saga/effects';
import { getBoard, putBoard, putSettings } from '../api';

import { makeGetBoardSuccess } from './actions';
import { GET_BOARD_REQUEST, POST_BOARD, POST_SETTINGS } from './types';


function* getBoardRequest({ payload }) {
    try {
        const { data } = yield call(getBoard, payload);
        yield put(makeGetBoardSuccess(data));
    } catch (e) {
        console.warn('getBoardRequest error: ', e);
    }
}

function* postBoard({ payload }) {
    try {
        const { data } = yield call(putBoard, { data: payload.data, id: payload.id });
        // yield put(makeGetBoardSuccess(data));
    } catch (e) {
        console.warn('postBoard error: ', e);
    }
}

function* postSettings({ payload }) {
    try {
        const { data } = yield call(putSettings, { data: payload.data, id: payload.id });
        // yield put(makeGetBoardSuccess(data));
    } catch (e) {
        console.warn('postBoard error: ', e);
    }
}

export default function* watchBoardSaga() {
    yield takeLatest(GET_BOARD_REQUEST, getBoardRequest);
    yield takeLatest(POST_BOARD, postBoard);
    yield takeLatest(POST_SETTINGS, postSettings);
};
