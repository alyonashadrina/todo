import * as types from './types';


const initialState = {
    board: {},
    isLoading: true,
};

export default function boardReducer(state = initialState, { type, payload }) {
    switch (type) {
    case types.GET_BOARD_REQUEST:
        return {
            ...state,
            isLoading: true,
        };
    case types.GET_BOARD_SUCCESS:
        return {
            ...state,
            board: payload,
            isLoading: false,
        };
    default:
        return state;
    }
}
