export const getBoardColumns = state => state.board.board.boardColumns;

export const getBoardSettings = state => state.board.board.settings;

export const isLoading = state => state.board.isLoading;
