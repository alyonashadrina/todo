import * as types from './types';

export const makeGetBoardRequest = () => ({
    type: types.GET_BOARD_REQUEST,
});

export const makeGetBoardSuccess = payload => ({
    type: types.GET_BOARD_SUCCESS,
    payload,
});