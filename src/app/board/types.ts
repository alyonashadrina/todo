export type taskType = {
    id: string,
    content: string
}

export type columnType = {
    id: string,
    title: string,
    taskIds: Array<string | number>,
}