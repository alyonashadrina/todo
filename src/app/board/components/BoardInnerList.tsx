import React, { memo } from "react";
import {columnType} from "../types";
import Column from "./Column";

type propType = {
    column: columnType,
    taskMap: any,
    index: number | string,
};

const BoardInnerList = memo((props: propType) => {
    const { column, taskMap, index } = props;

    const tasks = column.taskIds ? column.taskIds.map(taskId => taskMap[taskId]) : [];

    return (
        <Column
            column={column}
            tasks={tasks}
            index={index}
        />
    );
});

export default BoardInnerList;