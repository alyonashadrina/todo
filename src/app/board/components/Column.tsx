import React from 'react';
import {Draggable, Droppable} from 'react-beautiful-dnd';
import {createStyles, makeStyles, Paper, Theme} from "@material-ui/core";

import {columnType, taskType} from '../types';
import ColumnInnerList from "./ColumnInnerList";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            width: 300,
            margin: theme.spacing(1),
            padding: theme.spacing(1),
        },
        paperOver: {
        }
    }),
);

type propType = {
    column: columnType,
    index: number | string,
    tasks: Array<taskType>,
};

const Column = ({ column, index, tasks }: propType) => {

    const classes = useStyles();

    return (
        <Draggable draggableId={column.id} index={Number(index)}>
            {(providedParent, snapshot) => (
                <div {...providedParent.draggableProps} ref={providedParent.innerRef}>
                    <Droppable droppableId={column.id} type="task">
                        {(provided, snapshot) => (
                            <div
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                                // data-isdraggingover={snapshot.isDraggingOver.toString()}
                                data-isdraggingover={snapshot.isDraggingOver.toString()}
                            >
                                <Paper className={`${classes.paper} ${( snapshot.isDraggingOver ? classes.paperOver : "")}`}>
                                <h2 {...providedParent.dragHandleProps}>
                                    {column.title}
                                </h2>
                                <ColumnInnerList tasks={tasks} />

                                {provided.placeholder}
                                </Paper>
                            </div>
                        )}
                    </Droppable>
                </div>
            )}
        </Draggable>
    )
};

export default Column
