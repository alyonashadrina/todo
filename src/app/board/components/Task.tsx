import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { createStyles, makeStyles, Theme } from "@material-ui/core";

import { taskType } from '../types';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        card: {
        }
    }),
);

type propType = {
    task: taskType,
    index: number | string,
}

const Task = ({ task, index }: propType) => {
    
    const classes = useStyles();

    return (
        <Draggable draggableId={task.id} index={Number(index)}>
            {provided => (
                <div
                    className={classes.card}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    {task.content}
                </div>
            )}
        </Draggable>
    );
};

export default Task