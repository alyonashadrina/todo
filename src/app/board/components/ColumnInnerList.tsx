import React, { memo } from "react";
import { taskType } from "../types";
import Task from "./Task";

type propType = {
    tasks: any,
}

const ColumnInnerList = memo((props: propType) => {
    const { tasks } = props;

    return tasks.map((task: taskType, index: number | string) => (
        <Task
            key={task.id}
            task={task}
            index={index}
        />
    ));
});

export default ColumnInnerList;