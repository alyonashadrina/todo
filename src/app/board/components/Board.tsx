import React, { useState, useEffect } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import Grid from '@material-ui/core/Grid';

import BoardInnerList from "./BoardInnerList";
import { taskType } from "../types";


type propType = {
    data: any,
    updateBoard(arg0: Array<taskType>): void,
};

type destinationType = {
    droppableId: string,
    index: number
};

const Board = ({ data, updateBoard }: propType) => {

    const [tasks, setTasks] = useState(data);

    useEffect(() => {
        setTasks(data);
    }, [data]);

    useEffect(() => {
        updateBoard(tasks)
    }, [tasks]);

    const onDragUpdate = (update: { destination?: destinationType }) => {
        const { destination } = update;
        const opacity = destination
            ? destination.index / Object.keys(tasks.tasks).length
            : 0;
        document.body.style.backgroundColor = `rgba( 153, 141, 217, ${opacity})`;
    };

    const onDragEnd = (result: {
        destination?: destinationType,
        source: destinationType,
        draggableId: string,
        type: string,
    }) => {
        const { destination, source, draggableId, type } = result;
        document.body.style.backgroundColor = 'inherit';
        if (!destination) {
            return;
        }

        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }

        if (type === 'column') {
            const newColumnOrder = Array.from(tasks.columnOrder);
            newColumnOrder.splice(source.index, 1);
            newColumnOrder.splice(destination.index, 0, draggableId);

            const newState = {
                ...tasks,
                columnOrder: newColumnOrder,
            };
            setTasks(newState);
            return;
        }

        const home = tasks.columns[source.droppableId];
        const foreign = tasks.columns[destination.droppableId];

        if (home === foreign) {
            const newTaskIds = Array.from(home.taskIds);
            newTaskIds.splice(source.index, 1);
            newTaskIds.splice(destination.index, 0, draggableId);

            const newHome = {
                ...home,
                taskIds: newTaskIds,
            };

            const newState = {
                ...tasks,
                columns: {
                    ...tasks.columns,
                    [newHome.id]: newHome,
                },
            };
            setTasks(newState)
            return;
        }

        // moving from one list to another
        const homeTaskIds = Array.from(home.taskIds);
        homeTaskIds.splice(source.index, 1);
        const newHome = {
            ...home,
            taskIds: homeTaskIds,
        };

        const foreignTaskIds = Array.from(foreign.taskIds || []);
        foreignTaskIds.splice(destination.index, 0, draggableId);
        const newForeign = {
            ...foreign,
            taskIds: foreignTaskIds,
        };

        const newState = {
            ...tasks,
            columns: {
                ...tasks.columns,
                [newHome.id]: newHome,
                [newForeign.id]: newForeign,
            },
        };
        setTasks(newState)
    };

    return (
        <DragDropContext
            onDragEnd={onDragEnd}
            onDragUpdate={onDragUpdate}
        >
            <Droppable
                droppableId="all-columns"
                direction="horizontal"
                type="column"
            >
                {
                    provided => (
                        <Grid
                            container
                            justify="flex-start"
                            wrap="nowrap"
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                        >
                            {
                                tasks && tasks.columnOrder.map((columnId: number | string, i: number) => {
                                    const column = tasks.columns[columnId];
                                    return (
                                        <BoardInnerList
                                            key={column.id}
                                            column={column}
                                            taskMap={tasks.tasks}
                                            index={i}
                                        />
                                    );
                                })
                            }
                            {provided.placeholder}
                        </Grid>
                    )
                }
            </Droppable>
        </DragDropContext>
    );
};

export default Board