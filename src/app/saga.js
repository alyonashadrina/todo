import { all } from 'redux-saga/effects';

import boardSaga from './board/state/saga';
import boardListSaga from './boardList/state/saga';
import authSaga from './auth/state/saga';


export default function* rootSaga() {
    yield all([
        boardSaga(),
        boardListSaga(),
        authSaga(),
    ]);
}
