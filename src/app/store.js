import { createStore, compose, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { reactReduxFirebase } from 'react-redux-firebase'

import rootSaga from './saga'
import rootReducer from "./reducer";

import firebase from '../config/firebase'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();

const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase),
    composeEnhancer(applyMiddleware(sagaMiddleware)),
)(createStore);

const store = createStoreWithFirebase(rootReducer, {});


// const store = createStore(
//     rootReducer,
//     composeEnhancer(applyMiddleware(sagaMiddleware)),
//     // applyMiddleware(sagaMiddleware),
// );

sagaMiddleware.run(rootSaga);

export default store;