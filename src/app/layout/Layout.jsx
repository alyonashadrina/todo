import CssBaseline from '@material-ui/core/CssBaseline';
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect } from "react-router-dom";
import { ThemeProvider } from '@material-ui/styles';

import ButtonAppBar from './AppBar';
import Board from '../../pages/Board';
import Account from '../../pages/Account';
import Login from '../../pages/Auth';

import MuiTheme from '../../style';

const routes = [
    {
        path: "/board/:id",
        component: Board,
        exact: false,
        private: true,
    },
    {
        path: "/",
        component: Account,
        exact: true,
        private: true,
    },
    {
        path: "/login",
        component: Login,
        exact: true,
        private: false,
    },
];

const PrivateRoute = ({ component: Component, ...rest }) => {
    const apiKey = useSelector(state => state.firebase.auth.apiKey);
    const [isLoggedIn, setLoggedIn] = useState(false);

    useEffect(() => {
        setLoggedIn(!!apiKey);
        console.log('apiKey', apiKey)
        console.log('isLoggedIn', isLoggedIn)
    }, [apiKey, isLoggedIn]);

    return (
        <Route
            {...rest}
            render={(props) => (
                isLoggedIn
                    ? <Component {...props} />
                    : <Redirect to='/login' {...props} />
            )}
        />
    )
}

const Layout = () => {
    return (
        <ThemeProvider theme={MuiTheme}>
            <CssBaseline />
            <ButtonAppBar />
            {
                routes.map( route => {
                    if (route.private) {
                        return <PrivateRoute key={ route.path } { ...route } />
                    }
                    return <Route key={ route.path } { ...route } />
                })
            }
        </ThemeProvider>
    );
};

export default Layout;

