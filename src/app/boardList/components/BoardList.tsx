import {Link} from "react-router-dom";
import React, {ChangeEvent, useState} from "react";
import { Grid, Paper, makeStyles, createStyles, Theme, Button, Input } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: theme.spacing(4),
        },
        paper: {
            minHeight: 83,
            padding: theme.spacing(2),
            transition: `background-color 0.${theme.transitions.duration.short}s`,
            '&:hover': {
                backgroundColor: theme.palette.secondary.light,
            },
        },
        input: {
            marginBottom: theme.spacing(1),
            display: 'block',
        }
    }),
);

type propType = {
    boards: {},
    addBoard(arg0: string): void,
};

const BoardList = ({ boards, addBoard }: propType) => {
    const classes = useStyles();

    const [newBoardName, setNewBoardName] = useState('');

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setNewBoardName(e.target.value);
    };

    const addNewBoardName = () => {
        if (newBoardName) {
            addBoard(newBoardName);
        }
    };

    return (
        <Grid container spacing={2} className={classes.container}>
            {
                boards && Object.keys(boards).map((keyName) => (
                    <Grid
                        item
                        xs={12}
                        md={6}
                        lg={3}
                        key={keyName}
                    >
                        <Link to={'/board/' + keyName}>
                            <Paper
                                className={classes.paper}
                            >
                                <span className="input-label">{boards[keyName].settings.name}</span>
                            </Paper>
                        </Link>
                    </Grid>
                ))
            }
            <Grid
                item
                xs={12}
                md={6}
                lg={3}
            >
                <Input
                    value={newBoardName}
                    onChange={handleChange}
                    placeholder="Enter new board name"
                    className={classes.input}
                />
                <Button
                    size="large"
                    color="secondary"
                    variant="outlined"
                    onClick={addNewBoardName}
                >
                    add board
                </Button>
            </Grid>
        </Grid>
    )
};

export default BoardList;