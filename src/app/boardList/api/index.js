import axios from 'axios';
import { BASE_API_URI, API_URIS } from '../../../config/index';

export const getBoards = (data) => axios.get(BASE_API_URI + API_URIS.board + '.json');

export const addBoard = (data) => axios.post(BASE_API_URI + API_URIS.board + '.json', data);

