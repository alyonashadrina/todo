import * as types from './types';


const initialState = {
    boardId: '',
    boardList: {},
};

export default function boardReducer(state = initialState, { type, payload }) {
    switch (type) {
    case types.GET_BOARDS_SUCCESS:
        return {
            ...state,
            boardList: payload,
        };
    case types.ADD_BOARD_SUCCESS:
        return {
            ...state,
            boardId: payload,
        };
    default:
        return state;
    }
}
