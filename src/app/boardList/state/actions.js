import * as types from './types';

export const makeGetBoardsSuccess = payload => ({
    type: types.GET_BOARDS_SUCCESS,
    payload,
});

export const makeAddBoardSuccess = payload => ({
    type: types.ADD_BOARD_SUCCESS,
    payload,
});