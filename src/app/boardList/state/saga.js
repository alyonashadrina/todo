import { call, put, takeLatest } from 'redux-saga/effects';
import { addBoard, getBoards } from '../api';

import { makeAddBoardSuccess, makeGetBoardsSuccess } from './actions';
import { ADD_BOARD, GET_BOARDS } from './types';

function* getAllBoards({ payload }) {
    try {
        const { data } = yield call(getBoards, payload);
        yield put(makeGetBoardsSuccess(data));
    } catch (e) {
        console.warn('postBoard error: ', e);
    }
}

function* postBoard({ payload }) {
    try {
        const { data } = yield call(addBoard, payload);
        yield put(makeAddBoardSuccess(data.name));
    } catch (e) {
        console.warn('postBoard error: ', e);
    }
}

export default function* watchBoardSaga() {
    yield takeLatest(GET_BOARDS, getAllBoards);
    yield takeLatest(ADD_BOARD, postBoard);
};
