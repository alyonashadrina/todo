import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";

import { getBoardId, getBoards } from "../state/selectors";
import { ADD_BOARD, GET_BOARDS } from "../state/types";
import { BOARD_BOILERPLATE } from "../../../config/index";
import BoardList from "../components/BoardList";


const BoardListContainer = ({ history }: RouteComponentProps) => {

    const dispatch = useDispatch();

    // get list of boards
    useEffect(() => {
        dispatch({type: GET_BOARDS});
    }, []);
    const boardsState = useSelector(state => getBoards(state));
    const [boards, setBoards] = useState({});
    useEffect(() => {
        setBoards(boardsState);
    }, [boardsState]);

    // create new board and route to it
    const addBoard = (name: string) => {
        dispatch({
            type: ADD_BOARD,
            payload: {...BOARD_BOILERPLATE, settings: { name: name }}
        });
    };
    const boardId = useSelector((state: { boardList: {}}) => getBoardId(state));
    useEffect(() => {
        if (boardId) {
            history.push('/board/' + boardId)
        }
    }, [boardId]);


    const profile = useSelector((state: { firebase: {}}) => state.firebase);
    console.log('profile', profile);

    return (
        <div>
            <BoardList boards={boards} addBoard={addBoard}/>
        </div>
    )
};

export default withRouter(BoardListContainer);
