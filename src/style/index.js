import { createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import indigo from '@material-ui/core/colors/indigo';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: grey[50],
        },
        secondary: {
            light: indigo[50],
            main: indigo[500],
            dark: indigo[600],
        },
    },
    overrides: {
        MuiGrid: {
            container: {
                maxWidth: '100%',
            }
        }
    },
});
export default theme;